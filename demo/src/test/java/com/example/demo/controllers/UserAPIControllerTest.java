package com.example.demo.controllers;

import com.example.demo.models.entities.User;
import com.example.demo.models.repositories.UserRepository;
import com.example.demo.models.request.UserListAPIRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserAPIController.class)
public class UserAPIControllerTest {
  
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private UserRepository userRepo;

  @Test
  void whenValidInput_thenReturns200() throws Exception {
    List<User> list = new ArrayList<>();
    list.add(new User(1, "Hello", "World"));
    doReturn(list).when(this.userRepo).findAll(any(Pageable.class), any(UserListAPIRequest.class));
    
    mockMvc.perform(get("/api/users").param("limit", "10")   
    .contentType("application/json"))
    .andExpect(jsonPath("$.data", hasSize(1)))
    .andExpect(jsonPath("$.data[0].id", is(1)))
    .andExpect(jsonPath("$.data[0].username", is("Hello")))
    .andExpect(jsonPath("$.data[0].password", is("World")))
    .andExpect(status().isOk());
  }

  @Test
  void whenInvalidInput_thenReturns200() throws Exception {
    mockMvc.perform(get("/api/users").param("limit", "-1")   
    .contentType("application/json"))
    .andExpect(jsonPath("$.statusCode", is(400)))
    .andExpect(jsonPath("$.errors", hasSize(1)))
    .andExpect(jsonPath("$.errors[0].field", is("limit")))
    .andExpect(jsonPath("$.errors[0].message", is("Limit should not be less than 1")))
    .andExpect(status().isOk());
  }
}
