package com.example.demo.models.request;

import javax.validation.constraints.Min;

import com.example.demo.core.constraints.ValueOfEnum;
import com.example.demo.models.enums.OrderSequences;
import com.example.demo.models.enums.UserOrderEnums;

public class UserListAPIRequest {
  private String keyword = "";

  @Min(value = 0, message = "Page should not be less than 0")
  private Integer page = 0;

  @Min(value = 1, message = "Limit should not be less than 1")
  private Integer limit = 10;

  @ValueOfEnum(enumClass=UserOrderEnums.class, message = "Order By should be any of [id, username, created_on, last_login]")
  private String orderBy = "id";

  @ValueOfEnum(enumClass=OrderSequences.class, message = "Order should be any of [asc, desc, ASC, DESC]")
  private String order = "asc";

  private UserListAPIRequest() {}

  public String getKeyword() {
      return keyword;
  }

  public void setKeyword(String keyword) {
      this.keyword = keyword;
  }

  public Integer getPage() {
      return page;
  }

  public void setPage(Integer page) {
      this.page = page;
  }

  public Integer getLimit() {
      return limit;
  }

  public void setLimit(Integer limit) {
      this.limit = limit;
  }

  public String getOrderBy() {
      return orderBy;
  }

  public void setOrderBy(String orderBy) {
      this.orderBy = orderBy;
  }

  public String getOrder() {
      return order;
  }

  public void setOrder(String order) {
      this.order = order;
  }
}
