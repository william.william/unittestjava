package com.example.demo.models.enums;

public enum UserOrderEnums {
  id("id"),
  username("username"),
  created_on("created_on"),
  last_login("last_login")
  ;

  private final String text;

  /**
   * @param text
   */
  UserOrderEnums(final String text) {
      this.text = text;
  }

  /* (non-Javadoc)
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString() {
      return text;
  }
}
