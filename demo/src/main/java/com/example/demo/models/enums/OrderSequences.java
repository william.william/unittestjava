package com.example.demo.models.enums;

public enum OrderSequences {
  asc("asc"),
  desc("desc"),
  ASC("ASC"),
  DESC("DESC")
  ;

  private final String text;

  /**
   * @param text
   */
  OrderSequences(final String text) {
      this.text = text;
  }

  /* (non-Javadoc)
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString() {
      return text;
  }
}
