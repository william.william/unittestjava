package com.example.demo.models.response;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.models.basic.ErrorObject;
import com.example.demo.models.entities.User;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class UserListAPIResponse {
  private Integer statusCode;
  private String message;
  private List<User> data;
  private List<ErrorObject> errors;

  public UserListAPIResponse(){
    this.statusCode = 200;
    this.message = "";
    this.data = null;
    this.errors = null;
  }

  public UserListAPIResponse(Integer statusCode, String message, List<User> data, List<ObjectError> errors){
    this.statusCode = statusCode;
    this.message = message;
    this.data = data;
    this.setErrors(errors);
  }

  public void setStatusCode(Integer statusCode){
    this.statusCode = statusCode;
  }
  public Integer getStatusCode(){
    return statusCode;
  }
  public void setMessage(String message){
    this.message = message;
  }
  public String getMessage(){
    return message;
  }
  public void setData(List<User> data){
    this.data = data;
  }
  public List<User> getData(){
    return data;
  }
  public void setErrors(List<ObjectError> errors){
    this.errors = null;
    if(errors != null){
      this.errors = new ArrayList<>();
      for(ObjectError obj : errors){
        this.errors.add(new ErrorObject(((FieldError)obj).getField(), obj.getDefaultMessage()));
      }
    }
  }
  public List<ErrorObject> getErrors(){
    return errors;
  }
}
