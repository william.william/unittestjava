package com.example.demo.models.repositories;
 
import java.util.List;

import com.example.demo.models.entities.User;
import com.example.demo.models.request.UserListAPIRequest;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
 
public interface UserRepository extends CrudRepository<User, Integer> {

  @Query("select u from User u where u.username = ?1 and u.password = ?2")
  User findByUsernameAndPassword(String username, String password);

  @Query("select u from User u where u.username = ?1")
  User findByUsername(String username);

  @Query("select u from User u where u.username like %?#{#req.keyword}% ")
  List<User> findAll(Pageable pageable, @Param("req") UserListAPIRequest req);
}