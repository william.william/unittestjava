package com.example.demo.controllers;

import com.example.demo.models.entities.User;
import com.example.demo.models.response.UserListAPIResponse;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.demo.models.repositories.UserRepository;
import com.example.demo.models.request.UserListAPIRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

import javax.validation.Valid;

@RestController
public class UserAPIController {

  @Autowired
  private UserRepository userRepo;

  @GetMapping("/api/users") 
	public UserListAPIResponse getUserList(@Valid UserListAPIRequest req, BindingResult bindingResult) {
    if(bindingResult.hasErrors()){
      return new UserListAPIResponse(400, "Error", null,bindingResult.getAllErrors());  
    }
    Sort sort = req.getOrder().equalsIgnoreCase("desc") ? Sort.by(req.getOrderBy()).descending() : Sort.by(req.getOrderBy()).ascending();
    Pageable pageable = PageRequest.of(req.getPage(), req.getLimit(), sort);
    List<User> data = userRepo.findAll(pageable, req);
    return new UserListAPIResponse(200, "Success", data, null);
	}
}
